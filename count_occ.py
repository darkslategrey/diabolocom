#!/usr/bin/env python3
import fileinput
import operator

def main():
    d = {}
    with fileinput.input() as f:
        for line in f:
            for elt in line.split():
                n = d.get(elt, 0)
                d[elt] = n + 1
    res = sorted(d.items(), key=operator.itemgetter(1), reverse=True)
    for occ in res:
        print("%s\t%s" % (occ[0], occ[1]))




if __name__ == '__main__':
    main()

