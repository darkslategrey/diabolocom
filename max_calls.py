#!/usr/bin/env python3

import sys


def main(input_file):
    calls = []
    min_start = 99999999999
    max_end   = -1
    input_file =  open(input_file,'r')
    line_nbr   = 1
    '''
    In this loop: 

    1/ We read the input file
    2/ We set the minimum starting second and the maximum ending second
    3/ We also build a data structure to represent the input data.
    This structure is a list of dicts where each one have two keys/values:
    start_call and end_call

    '''
    for line in input_file:
        start_call = end_call = 0
        try:
            start_call, end_call = [ int(elt) for elt in line.split(':') ]
        except ValueError as err:
            msg = "ERROR: invalid input line '%s' at line %d"%(line.strip(),line_nbr)
            print(msg, file=sys.stderr)
            continue
        calls.append({ 'start_call': start_call, 'end_call': end_call })
        min_start = start_call if start_call < min_start else min_start
        max_end   = end_call   if end_call   > max_end   else max_end
        line_nbr += 1
    input_file.close()

    max_calls = 0
    
    '''
    For each second from the minimum up to the maximum,
    we count the number of call's interval which contains the current
    second.
    The result is the max number of calls found.
    '''
    for second in range(min_start, max_end):
        calls_nbr = 0
        for call in calls:
            if second >= call['start_call'] and second <= call['end_call']:
                calls_nbr += 1
        max_calls = calls_nbr if calls_nbr > max_calls else max_calls

    print("Max calls observed in one second is %d"%max_calls)


def print_help(script_name):
    msg = """
    usage: %s input_file

    This script accepts a file as argument which
    contains a list of calls in the following format:

    1385718405:1385718491
    1385718407:1385718409
    ...

    It return the maximum calls observed in one second.
    """%script_name
    print(msg)

if __name__ == '__main__':
    
    if len(sys.argv) != 2:
        print_help(sys.argv[0])
        exit(0)
    try:
        main(input_file=sys.argv[1])
    except OSError as err:
        print("{0}".format(err), file=sys.stderr)
        


